//
//  InAppPurchaseViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 4/23/14.
//
//

#import "InAppPurchaseViewController.h"
#import "DataHolder.h"
#import "InAppPurchaseManager.h"
#import "Chartboost.h"
@interface InAppPurchaseViewController ()

@end

@implementation InAppPurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     self.btn1.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    self.btn2.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    self.btn3.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    self.btn4.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    self.btn5.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];

    self.btn6.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    self.btn7.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    self.moreBtn.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)PurchaseProduct:(id)sender{

    [[InAppPurchaseManager InAppPurchaseManagerSharedInstance] PurchaseProductWithNumber:[sender tag] Delegate:self WithSelector:@selector(Purchased:) WithErrorSelector:@selector(Error:)];
}
-(IBAction)Restore:(id)sender{

    [[InAppPurchaseManager InAppPurchaseManagerSharedInstance] Restore_ProductsWithDelegate:self WithSelector:@selector(Purchased:) WithErrorSelector:@selector(Error:)];
}
-(void)Purchased:(NSString*)product{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ads"];
    
    if ([product isEqualToString:Coins_500]) {
        [self AddCoins:500];
    }
    if ([product isEqualToString:Coins_1000]) {
        [self AddCoins:1000];
    }
    if ([product isEqualToString:Coins_3000]) {
        [self AddCoins:3000];
    }
    if ([product isEqualToString:Coins_8000]) {
        [self AddCoins:8000];
    }
    if ([product isEqualToString:Coins_25000]) {
        [self AddCoins:25000];
    }
    if ([product isEqualToString:remove_ads]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"removeads"];
    }
    
    //updating label only.
}
-(void)AddCoins:(int)coins{

    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        int coins=[[user objectForKey:@"coins"] integerValue]+coins;
        user[@"coins"]=[NSNumber numberWithInt:coins];
        [user save];
    }];

}
-(void)Error:(NSError*)error{
    
    // [[AppManager AppManagerSharedInstance ] Show_Alert_With_Title:@"Error!" message:@"Unexpected Error Occured.Please Try Again Later"];
}

-(IBAction)Back:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
   // [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)moreAds:(id)sender{
    Chartboost* cb = [Chartboost sharedChartboost];
    cb.delegate = self;
    [cb cacheMoreApps];
    [cb showMoreApps];
}
@end
