//
//  LikeViewController.h
//  ParseStarterProject
//
//  Created by Kanwal on 4/21/14.
//
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "PHAPIRequest.h"

@interface LikeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,GADBannerViewDelegate,PHAPIRequestDelegate>{

    IBOutlet UIImageView *ImageView;
    IBOutlet UIButton *skipBtn;
    IBOutlet UIButton *likeBtn;
    IBOutlet UIButton *reloadBtn;
    IBOutlet UIView *getlikesView;
    IBOutlet UITableView *table;
    IBOutlet UILabel *coinsLabel;
    IBOutlet UISegmentedControl *segment;
    IBOutlet UIButton *getlikesBtn;
    IBOutlet UIButton *getcoinsBtn;
    IBOutlet UIActivityIndicatorView *act;
    IBOutlet UIActivityIndicatorView *act2;
    IBOutlet UIButton *reloadUserMedia;

    NSArray *DataArray;
}
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;

@end
