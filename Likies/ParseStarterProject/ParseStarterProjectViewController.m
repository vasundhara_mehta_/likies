#import "ParseStarterProjectViewController.h"
#import <Parse/Parse.h>
#import "IGLoginViewController.h"

@implementation ParseStarterProjectViewController


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - UIViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self.btn.titleLabel setFont:[UIFont fontWithName:@"Dosis-Light" size:13]];
}
-(IBAction)LoginIG:(id)sender{

    IGLoginViewController *Obj=[[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:nil];
   // [self.navigationController pushViewController:Obj animated:YES];
    [self presentViewController:Obj animated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
